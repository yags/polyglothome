let modalMethod = document.getElementById('modalMethod');

let btnMethod = document.getElementById("methodAction");
btnMethod.onclick = function (event) {
    event.preventDefault();
    modalMethod.style.display = "block";
}

let spanMethod = modalMethod.getElementsByClassName("close")[0];
spanMethod.onclick = function () {
    modalMethod.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//     if (event.target == modalMethod) {
//         modalMethod.style.display = "none";
//     }
// }
