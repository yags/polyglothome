<link rel="stylesheet" href="method/method.css">

<div id="modalMethod" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
        </div>
        <div class="container p-3 no-p-sides">
            <div class="twelve columns p-1">
                <div class="seven columns">
                    <div class="u-pull-left font-black font-bold font-italic">
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
                        Corporis quidem hic nemo ut. Asperiores vitae ut voluptatum eaque beatae officiis consectetur. 
                        Temporibus quaerat harum placeat vero, maxime dolores sint architecto?
                    </div>
                </div>
            </div>
            <div class="twelve columns p-1">
                <div class="seven columns">
                    <h6>1. Lorem ipsum dolor sit amet:</h6>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Asperiores odit ullam ad ea, fuga ipsum alias similique? 
                    Quasi, quaerat veniam. 
                    Dignissimos, eius veniam id delectus quos doloribus voluptate quis similique?
                </div>
            </div>
            <div class="twelve columns p-1">
                <div class="seven columns">
                    <h6>2. Lorem ipsum dolor sit amet:</h6>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Asperiores odit ullam ad ea, fuga ipsum alias similique? 
                    Quasi, quaerat veniam. 
                    Dignissimos, eius veniam id delectus quos doloribus voluptate quis similique?
                </div>
            </div>
            <div class="twelve columns p-1">
                <div class="seven columns">
                    <h6>3. Lorem ipsum dolor sit amet:</h6>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Asperiores odit ullam ad ea, fuga ipsum alias similique? 
                    Quasi, quaerat veniam. 
                    Dignissimos, eius veniam id delectus quos doloribus voluptate quis similique?
                </div>
            </div>
            <div class="twelve columns p-1">
                <div class="seven columns">
                    <h6>3. Lorem ipsum dolor sit amet:</h6>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Asperiores odit ullam ad ea, fuga ipsum alias similique? 
                    Quasi, quaerat veniam. 
                    Dignissimos, eius veniam id delectus quos doloribus voluptate quis similique?
                </div>
            </div>
        </div>
    </div>
</div>

<script src="method/method.js"></script>
