let modalStats = document.getElementById('modalStats');

let btnStats = document.getElementById("statsAction");
btnStats.onclick = function (event) {
    event.preventDefault();
    modalStats.style.display = "block";
}

let spanStats = modalStats.getElementsByClassName("close")[0];
spanStats.onclick = function () {
    modalStats.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//     if (event.target == modalStats) {
//         modalStats.style.display = "none";
//     }
// }
