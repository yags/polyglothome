<link rel="stylesheet" href="stats/stats.css">

<div id="modalStats" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h4>Estadísticas</h4>
            <hr>
        </div>
        <div class="twelve columns p-3 pt-0 d-flex jc-center flex-wrap">
            <div class="one-third column text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <img class="u-max-full-width mt-1" src="../commons/images/stats1.png" alt="Entrenamiento" width="50%">
                        </div>
                        <span>Total acumulado</span><br>
                        <span id="stats1">{{ stats 1 }}</p>
                    </div>
                    <div class="card-footer p-1 no-p-sides" style="background-color:#f8316f;">
                        <h5>Entrenamiento</h5>
                    </div>
                </div>
            </div>
            <div class="one-third column text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <img class="u-max-full-width mt-1" src="../commons/images/stats2.png" alt="Bits" width="50%">
                        </div>
                        <span>Total acumulado</span><br>
                        <span id="stats2">{{ stats 2 }}</p>
                    </div>
                    <div class="card-footer p-1 no-p-sides" style="background-color:#8173fa;">
                        <h5>Bits</h5>
                    </div>
                </div>
            </div>
            <div class="one-third column text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <img class="u-max-full-width mt-1" src="../commons/images/stats3.png" alt="Clases Vistas" width="50%">
                        </div>
                        <span>Total acumulado</span><br>
                        <span id="stats3">{{ stats 3 }}</p>
                    </div>
                    <div class="card-footer p-1 no-p-sides" style="background-color:#27f2cb;">
                        <h5>Clases Vistas</h5>
                    </div>
                </div>
            </div>
            <div class="one-third column text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <img class="u-max-full-width mt-1" src="../commons/images/stats4.png" alt="Bits del Vídeo" width="50%">
                        </div>
                        <span>Total acumulado</span><br>
                        <span id="stats4">{{ stats 4 }}</p>
                    </div>
                    <div class="card-footer p-1 no-p-sides" style="background-color:#8ddd47;">
                        <h5>Bits del Vídeo</h5>
                    </div>
                </div>
            </div>
            <div class="one-third column text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <img class="u-max-full-width mt-1" src="../commons/images/stats5.png" alt="Tiempo del Vídeo" width="50%">
                        </div>
                        <span>Total acumulado</span><br>
                        <span id="stats5">{{ stats 5 }}</p>
                    </div>
                    <div class="card-footer p-1 no-p-sides" style="background-color:#ffa022;">
                        <h5>Tiempo del Vídeo</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="stats/stats.js"></script>
