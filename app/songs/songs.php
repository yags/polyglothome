<link rel="stylesheet" href="songs/songs.css">

<div id="modalSongs" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <div class="d-flex">
                <h4 id="songTitle">
                    {{song_title}}
                </h4>
                <div class="d-flex ai-center flags">
                    <img src="../commons/images/spain.png" alt="Spain" width="40">
                    <img src="../commons/images/uk.png" alt="United Kingdom" width="40">
                    <span class="close">&times;</span>
                </div>
            </div>
            <hr>
        </div>
        
        <div class="twelve columns p-3 pb-1 pt-0">
            <video src="#" id="mainSong">
            </video>
        </div>
        
        <div style="padding: 0 16px;">
            <hr>
        </div>
        
        <div class="twelve columns modal-footer d-flex jc-center p-3 pt-0 songList">    
            <div class="one-third column">
                <div class="video-card d-flex ai-center jc-center">
                    <img src="../commons/images/play.png" width="60">
                </div>
                {{ song_1 }}
            </div>
            <div class="one-third column">
                <div class="video-card d-flex ai-center jc-center active-video">
                    <img src="../commons/images/play.png" width="60">
                </div>
                {{ song_2 }}
            </div>
            <div class="one-third column">
                <div class="video-card d-flex ai-center jc-center">
                    <img src="../commons/images/play.png" width="60">
                </div>
                {{ song_3 }}
            </div>
        </div>
    </div>
</div>

<script src="songs/songs.js"></script>
