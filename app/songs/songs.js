let modalSongs = document.getElementById('modalSongs');

let btnSongs = document.getElementById("songsAction");
btnSongs.onclick = function (event) {
    event.preventDefault();
    modalSongs.style.display = "block";
}

let spanSongs = modalSongs.getElementsByClassName("close")[0];
spanSongs.onclick = function () {
    modalSongs.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//     if (event.target == modalSongs) {
//         modalSongs.style.display = "none";
//     }
// }
