<link rel="stylesheet" href="downloads/downloads.css">
<div id="modalDownload" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>  
            <h4>Descargas</h4>
            <hr>
        </div>
        <div class="container pb-3">
            <div id="downloadList">
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
                <div class="d-flex jc-space-between p-1 no-p-sides ai-center">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    <img src="../commons/images/download.png" alt="Descargar" width="25">
                </div>
            </div>
            <div class="d-flex jc-center">
                <img class="u-max-full-width" src="../commons/images/pavinchi.png" alt="Pavinchi" width="70%" height="50%">
            </div>
        </div>
    </div>
</div>

<script src="downloads/downloads.js"></script>
