let modalDownloads = document.getElementById('modalDownload');

let btnDownload = document.getElementById("downloadAction");
btnDownload.onclick = function (event) {
    event.preventDefault();
    modalDownloads.style.display = "block";
}

let spanDownload = modalDownloads.getElementsByClassName("close")[0];
spanDownload.onclick = function () {
    modalDownloads.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//     if (event.target == modalDownloads) {
//         modalDownloads.style.display = "none";
//     }
// }
