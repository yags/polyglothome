<div class="container footer p-1">
    <div class="twelve columns d-flex jc-center p-1">
        <a href="#">Términos y condiciones</a> &nbsp; | &nbsp;
        <a href="#">Políticas de datos</a> &nbsp; | &nbsp;
        <a href="#">Preguntas frecuentes</a>
    </div>
    <div class="twelve columns d-flex jc-center">
        Copyright 2019 Blappsis &nbsp; | &nbsp; Todos los derechos reservados
    </div>
</div>