<link rel="stylesheet" href="profile/profile.css">
<div id="modalProfile" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>  
      <h4>Mi perfil</h4>
      <hr>
    </div>
    <div class="container">
      <form action="" method="post" id="profileModal">
        <div class="twelve columns modal-body text-center">
          <div class="one-half column">
            <label for="names">
              Nombres  
              <input type="text" name="names" id="names" required>
            </label>
          </div>
          <div class="one-half column">
            <label for="lastNames">
              Apellidos
              <input type="text" name="lastNames" id="lastNames" required>
            </label>
          </div>
          <div class="one-half column">
            <label for="sonNames">
              Nombres hijo(a)
              <input type="text" name="sonNames" id="sonNames" required>
            </label>
          </div>
          <div class="one-half column">
            <label for="sonLastNames">
              Apellidos hijo(a)
              <input type="text" name="sonLastNames" id="sonLastNames" required>
            </label>
          </div>
          <div class="one-half column">
            <label for="email">
              E-mail
              <input type="email" name="email" id="email" required>
            </label>
          </div>
          <div class="one-half column">
            <label for="password">
              Contraseña
              <input type="password" maxlength="25" name="password" id="password" required>
            </label>
          </div>
          <div class="twelve column">
            <label for="birdthDate">
              Fecha de nacimiento
              <input type="date" name="birdthDate" id="birdthDate" required>
            </label>
          </div>
        </div>
        <div class="twelve columns modal-footer d-flex jc-center p-2">
          <button class="button-success" type="submit">Actualizar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="profile/profile.js"></script>
