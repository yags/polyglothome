let modalProfile = document.getElementById('modalProfile');

let btnProfile = document.getElementById("profileAction");
btnProfile.onclick = function (event) {
    event.preventDefault();
    modalProfile.style.display = "block";
}

let spanProfile = modalProfile.getElementsByClassName("close")[0];
spanProfile.onclick = function () {
    modalProfile.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//     if (event.target == modalProfile) {
//         modalProfile.style.display = "none";
//     }
// }
