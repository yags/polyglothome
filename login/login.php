<link rel="stylesheet" href="login/login.css">
<div class="container">
    <div class="twelve columns d-flex jc-center pt-3">
        <img class="u-max-full-width" src="commons/images/logo.png" alt="Polyglot World Home" width="200">
    </div>
    <div class="twelve columns pt-3 d-flex jc-center">
        <div class="card-login p-4">
            <form action="" method="post" class="text-center">
                <div class="twelve columns">
                    <input type="email" name="email" placeholder="E-mail" maxlength="30" required>
                </div>
                <div class="twelve columns">
                    <input type="password" name="password" placeholder="Contraseña" maxlength="20" required>
                </div>
                <div class="twelve columns mb-1">
                    <label class="containerLabel" for="remember_me" class="label-body">
                        Recordarme
                        <input type="checkbox" name="remember_me" id="remember_me">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="twelve columns mb-2">
                    <a href="#" class="">¿Haz olvidado tu contraseña?</a>
                </div>
                <div class="twelve columns mb-0">
                    <button type="submit" class="button-primary mb-0">Empezar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<img class="u-max-full-width pos-fixed" src="commons/images/logo_baby_brain.png" alt="Baby Brain" width="150" id="footer_picture">