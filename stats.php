<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Estadisticas</title>
    <link rel="stylesheet" href="commons/css/normalize.css">
    <link rel="stylesheet" href="commons/css/skeleton.css">
    <style>
        .card{
            background-color: #fff;
            border-radius: 1.5rem;
            font-weight: 700;
            width: 100%;
            box-shadow: #e0e0e0 0 0 10px;
            margin: 1rem 0;
        }
        .card-footer{
            width: 100%;
            color: #fff;
            border-bottom-left-radius: 1rem;
            border-bottom-right-radius: 1rem;
            word-break: break-all;
            word-wrap: break-word;
        }
    </style>
</head>
<body>
<div class="twelve columns p-3 pt-0 d-flex jc-center flex-wrap">
    <div class="one-third column text-center">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <img class="u-max-full-width mt-1" src="commons/images/stats1.png" alt="Entrenamiento" width="50%">
                </div>
                <span>Total acumulado</span><br>
                <span id="stats1">{{ stats 1 }}</p>
            </div>
            <div class="card-footer p-1 no-p-sides" style="background-color:#f8316f;">
                <h5>Entrenamiento</h5>
            </div>
        </div>
    </div>
    <div class="one-third column text-center">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <img class="u-max-full-width mt-1" src="commons/images/stats2.png" alt="Bits" width="50%">
                </div>
                <span>Total acumulado</span><br>
                <span id="stats2">{{ stats 2 }}</p>
            </div>
            <div class="card-footer p-1 no-p-sides" style="background-color:#8173fa;">
                <h5>Bits</h5>
            </div>
        </div>
    </div>
    <div class="one-third column text-center">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <img class="u-max-full-width mt-1" src="commons/images/stats3.png" alt="Clases Vistas" width="50%">
                </div>
                <span>Total acumulado</span><br>
                <span id="stats3">{{ stats 3 }}</p>
            </div>
            <div class="card-footer p-1 no-p-sides" style="background-color:#27f2cb;">
                <h5>Clases Vistas</h5>
            </div>
        </div>
    </div>
    <div class="one-third column text-center">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <img class="u-max-full-width mt-1" src="commons/images/stats4.png" alt="Bits del Vídeo" width="50%">
                </div>
                <span>Total acumulado</span><br>
                <span id="stats4">{{ stats 4 }}</p>
            </div>
            <div class="card-footer p-1 no-p-sides" style="background-color:#8ddd47;">
                <h5>Bits del Vídeo</h5>
            </div>
        </div>
    </div>
    <div class="one-third column text-center">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <img class="u-max-full-width mt-1" src="commons/images/stats5.png" alt="Tiempo del Vídeo" width="50%">
                </div>
                <span>Total acumulado</span><br>
                <span id="stats5">{{ stats 5 }}</p>
            </div>
            <div class="card-footer p-1 no-p-sides" style="background-color:#ffa022;">
                <h5>Tiempo del Vídeo</h5>
            </div>
        </div>
    </div>
</div>
</body>
</html>